let data = []
const state = {
    menuOpen: null,
    fav: null
}

fetch('/cafes')
    .then(response => response.json())
    .then(_data => {
        data = _data
        render()
    })
    .catch(err => console.error(err))


function getAddFormHtml() {
    return `
        <article>
            <form onsubmit="event.preventDefault();addCafe(this);">
                <label>Cafe Name</label>
                <input name="name" required />
                <label>Image URL for Cafe</label>
                <input name="image" type="url" required />
                <button style="width: 13rem;">Add Cafe</button>
            </form>
        </article>
    `
}

function render() {
    let content = data.map((cafeData, i) => {
        return `
            <article class="cafe-card">
                <div style="background-image: url('${cafeData.image}');"></div>
                <footer>
                    <h2 id="${i}-header">${cafeData.name}</h2>
                    <button onclick="displayMenus(${i})">Menus</button>
                <footer>
            </article>
        `
    }).join("")

    content += getAddFormHtml()

    const appEl = document.getElementById('app')
    appEl.innerHTML = content

    if (state.menuOpen) {
        const modalContent = `
            <section class="model-bg">
                <article>
                    ${state.menuOpen.map(menu => {
                        return `<h3>${menu.title}</h3>`
                    }).join("")}
                    <button onclick="closeModel()">close</button>
                </article>
            </section>
        `
        const modelEl = document.getElementById('model')
        modelEl.innerHTML = modalContent
    } else {
        const modelEl = document.getElementById('model')
        modelEl.innerHTML = ""
    }
}

function displayMenus(index) {
    state.menuOpen = data[index].menus
    render()
}

function addCafe(HTMLform) {
    const form = new FormData(HTMLform)
    const name = form.get('name')
    const image = form.get('image')
    fetch('/cafes', {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({name, image})
    })
    .then(res => res.json())
    .then(cafe => {
        data.push(cafe)
        render()
    })
    .catch(console.error)
}

function displayFav(id) {
    const title = document.getElementById(id)
    title.innerHTML += " ♥️"
}

function clearFav(id) {
    const title = document.getElementById(id)
    title.innerHTML = title.innerHTML.substring(0, title.innerHTML.length - 2)
}

function closeModel() {
    state.menuOpen = null
    render()
}
package io.multiverse.cafe.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import io.multiverse.cafe.Menu;

interface MenuRepo extends JpaRepository<Menu, Integer> {
    
}
